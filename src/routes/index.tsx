import { component$ } from "@builder.io/qwik";
import type { DocumentHead } from "@builder.io/qwik-city";
import Navbar from '../components/navbar'
export default component$(() => {
  return (
    <>
    <Navbar/>
    <img alt='banner' class='hero_img' src='/image-hero-mobile.png' />
      <h1>Make remote work</h1>
      <p>
      Get your team in sync, no matter your location. Streamline processes, 
  create team rituals, and watch productivity soar.
      </p>

      <button class='learn_more'>learn more</button>
    </>
  );
});

export const head: DocumentHead = {
  title: "Welcome to Qwik",
  meta: [
    {
      name: "description",
      content: "Qwik site description",
    },
  ],
};
