import { component$ } from "@builder.io/qwik";


export default component$( ()=>{
  return (
    <div class='container' >

    <nav class='nav' >
      <img class='logo' src="/logo.svg" alt='logo' />
      <div>
        <button>
          <img alt='menu' class='menu' src='/icon-menu.svg' />
        </button>
      </div>
    </nav>
    </div>
  )
})